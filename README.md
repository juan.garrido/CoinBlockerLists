![img](img/CoinBL_Logo.png)
## Simple lists that can help prevent cryptomining in the browser or other applications.

[![pipeline status](https://gitlab.com/ZeroDot1/CoinBlockerLists/badges/master/pipeline.svg)](https://gitlab.com/ZeroDot1/CoinBlockerLists/commits/master)
[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)
<img src="http://img.shields.io/liberapay/patrons/ZeroDot1.svg?logo=liberapay">

# Downloads
## [All downloads can be found on the CoinBlockerLists Homepage](https://zerodot1.gitlab.io/CoinBlockerListsWeb/downloads.html)

# Donations
Every donation helps me to continue the work on the CoinBlockerLists.
Would you like to donate?  If so, please click the yellow button.
[![img](img/AWL.png)](https://amzn.to/2rtogiP)

## RSS Feed [https://gitlab.com/ZeroDot1/CoinBlockerLists/commits/master?format=atom](https://gitlab.com/ZeroDot1/CoinBlockerLists/commits/master?format=atom)

# Check for false positives
- Note: With this tool you can check the CoinBlockerLists for false positives.
- If a domain is not in the CoinBlockerLists you want to add please contact me via email:  [zerodot1@bk.ru](mailto:zerodot1@bk.ru)
- https://malware-research.org/coinblockerlists/

Many thanks to [GelosSnake](https://twitter.com/GelosSnake) :)

# Cryptocurrency Address Database
- If you find crypto currency addresses you are welcome to add it to this table all users are welcome to contribute to this table.
- https://docs.google.com/spreadsheets/d/1YDojFsywBNXKST6luq3qQXBxraiaRTMOkYmb1SHVGyo/edit#gid=0

### CoinBlockerLists is now listed in FireHOL: [http://iplists.firehol.org/](http://iplists.firehol.org/)  
- [http://iplists.firehol.org/?ipset=coinbl_hosts](http://iplists.firehol.org/?ipset=coinbl_hosts)
- [http://iplists.firehol.org/?ipset=coinbl_hosts_optional](http://iplists.firehol.org/?ipset=coinbl_hosts_optional)
- [http://iplists.firehol.org/?ipset=coinbl_hosts_browser](http://iplists.firehol.org/?ipset=coinbl_hosts_browser)
- [http://iplists.firehol.org/?ipset=coinbl_ips](http://iplists.firehol.org/?ipset=coinbl_ips)

Many thanks to [https://github.com/ktsaou](https://github.com/ktsaou) :)

# More information about the project can be found on the CoinBlockerLists homepage: [https://zerodot1.gitlab.io/CoinBlockerListsWeb/](https://zerodot1.gitlab.io/CoinBlockerListsWeb/index.html)

# Contributing
You can contribute to the project as you want and you can also fork it but the changes must be public as the GPLv3 license specifies.
Thanks to all who use and support CoinBlockerLists.
[Issues](https://gitlab.com/ZeroDot1/CoinBlockerLists/issues/new) [Merge Requests](https://gitlab.com/ZeroDot1/CoinBlockerLists/merge_requests/new)

## Find sites infected with Coinhive: [http://whorunscoinhive.com/](http://whorunscoinhive.com/)
## Pi-hole - A black hole for Internet advertisements [https://github.com/pi-hole](https://github.com/pi-hole)

## References: [https://zerodot1.gitlab.io/CoinBlockerListsWeb/references.html](https://zerodot1.gitlab.io/CoinBlockerListsWeb/references.html)

## Partners
----------------------------------------------------------------------------------------------------------------
[![img](https://zerodot1.gitlab.io/CoinBlockerListsWeb/img/Deteque_Logo.png)](https://www.deteque.com/#)
### [Take a look at Deteque's great products.](https://www.deteque.com/#)
----------------------------------------------------------------------------------------------------------------
[![img](https://zerodot1.gitlab.io/CoinBlockerListsWeb/img/STLogo3.png)](https://securitytrails.com/)
### [DNS history, IP info and company information by SecurityTrails.](https://securitytrails.com/)
----------------------------------------------------------------------------------------------------------------
### Contact
You have a problem or want to change something in the CoinBlockerLists, please contact me. The following contact options are available.
- Twitter: [@hobbygrafix](https://twitter.com/hobbygrafix)
- Mastodon: [@katze](https://mastodon.social/@katze)
- E-Mail: [ZeroDot1@bK.Ru](mailto:zerodot1@bk.ru)
- Skype [This email address is only for Skype Chats and is NOT email support!]: [zerodot1@outlook.com](mailto:zerodot1@outlook.com)
