# Contribute: https://gitlab.com/ZeroDot1/CoinBlockerLists/issues/new
# Copyright: (c) ZeroDot1, 2018
# Description: This list contains broken or incorrect URLs.
# Donate: https://amzn.to/2rtogiP Every donation keeps the future development going. Thank you.
# Homepage: https://github.com/ZeroDot1/CoinBlockerLists/
# Last modified: 2018-09-10 00:10
# License: https://gitlab.io/ZeroDot1/CoinBlockerLists/LICENSE
# This file contains all invalid hosts
8jd2lfsq.me
abc.pema.cl
april-js.github.io
azvjudwr.info
browsealoud.com
cdn.rove.cl
cdnfile.xyz
cryweb.github.io
crywebber.github.io
gus.host
hans.pr0gramm.com
host.d-ns.ga
hurensohn.pr0gramm.com
jroqvbvw.info
jyhfuqoh.info
kdowqlpt.info
latimes-graphics-media.s3.amazonaws.com/js/leaflet.fullscreen-master/Control.FullScreen.js
lọgrhythm.com
m.pr0gramm.com
marta-js.github.io
mas-onjs.github.io
metrika.ron.si
mjija.github.io
piti.bplaced.net
st.kjli.fi
static.hk.rs
web.die-news.pw
xbasfbno.info
xy.nullrefexcep.com
